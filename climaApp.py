from tkinter import *
import requests
#34dc4b89c5a05836dc78e40b90d4da39
#https://api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}
#https://api.openweathermap.org/data/2.5/weather?q=Rosario&appid=34dc4b89c5a05836dc78e40b90d4da39

def mostrar_respuesta(clima):
    try:
        ciudad["text"] = clima["name"]
        descripcion["text"] = clima["weather"][0]["description"]
        temperatura["text"] = str(int(clima["main"]["temp"]))+" ºC"
    except:
        ciudad["text"] = "Intentelo nuevamente"

def clima_JSON(ciudad):
    try:
        API_Key = "34dc4b89c5a05836dc78e40b90d4da39"
        URL = "https://api.openweathermap.org/data/2.5/weather"
        parametros = {"APPID": API_Key, "q": ciudad, "units": "metric", "lang": "es"}
        response = requests.get(URL, params = parametros)
        clima = response.json()
        mostrar_respuesta(clima)
    except:
        print("Error")

ventana = Tk()
ventana.geometry("350x500")

texto_ciudad = Entry(ventana, font=("Courier", 20, "normal"), justify="center")
texto_ciudad.pack(padx = 30, pady = 30)

obtener_clima = Button(ventana, text="Obtener clima", font=("Courier", 20, "normal"), command=lambda: clima_JSON(texto_ciudad.get()))
obtener_clima.pack(padx = 30, pady = 30)

ciudad = Label(font=("Courier", 20, "normal"))
ciudad.pack(padx = 20, pady = 20)

temperatura = Label(font=("Courier", 50, "normal"))
temperatura.pack(padx = 10, pady = 10)

descripcion = Label(font=("Courier", 20, "normal"))
descripcion.pack(padx = 10, pady = 10)

ventana.mainloop()